<html>
    <head>
        <title>Laravel</title>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <ul>
                    <li><a href="/users">User</a></li>
                    <li><a href="/roles">Roles</a></li>
                    <li><a href="/activation">Activation</a></li>
                    <li><a href="/xml">XML</a></li>
                    <li><a href="/relations">Relations</a></li>
                    <li><a href="/sortable">Sortable</a></li>
                    <li><a href="/paginated">Relations - Paginated</a></li>
                    <li><a href="/search">Search - Append query string i.e; username=wanted_username</a></li>
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
            </div>
        </div>

        <div class="container">
        <div class="text-center">Sort on relationship example: http://localhost:8000/sortable?sort_by=last_name</div>
            <div class="text-center">Search example: http://localhost:8000/search?email=ivan</div>
            <div class="text-center">Search example: http://localhost:8000/search?last_name=Sammartino</div>
        </div>
    </body>
</html>
