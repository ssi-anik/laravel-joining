<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    protected $table = 'users';

    protected $fillable = ['name', 'email', 'password'];

    protected $hidden = ['password', 'remember_token'];

    public function roles(){
        return $this->belongsToMany('App\Role');
    }

    public function activation(){
        return $this->hasOne('App\Activation', 'user_id', 'id');
    }

    public function scopeSearchUserName($query, $username){
        if(empty($name)){
            return;
        }

        return $query->where('username', 'LIKE', sprintf("%%%s%%", $username));
    }

    public function scopeSearchEmail($query, $email){
        if(empty($email)){
            return;
        }

        return $query->where('email', 'LIKE', sprintf("%%%s%%", $email));
    }

    public function scopeSearchFirstName($query, $first_name){
        if(empty($first_name)){
            return;
        }

        return $query->where('first_name', 'LIKE', sprintf("%%%s%%", $first_name));
    }

    public function scopeSearchLastName($query, $last_name){
        if(empty($last_name)){
            return;
        }

        return $query->where('last_name', 'LIKE', sprintf("%%%s%%", $last_name));
    }
}
