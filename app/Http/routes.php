<?php

Route::get('/', 'HomeController@home');
Route::get('xml', 'HomeController@xml');
Route::get('users', 'HomeController@users');
Route::get('roles', 'HomeController@roles');
Route::get('activation', 'HomeController@activation');
Route::get('relations', 'HomeController@relations');
Route::get('sortable', 'HomeController@sortable');
Route::get('paginated', 'HomeController@paginated');
Route::get('search', 'HomeController@search');
