<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use App\Activation;

class HomeController extends Controller
{

    private $sortable_fields = [
        'first_name' => 'users.first_name',
        'last_name' => 'users.last_name',
        'email' => 'users.email',
        'role' => 'roles.name',
    ];

    public function home(){
        return view('welcome');
    }

    public function users(){
        return App\User::all();
    }

    public function roles(){
        return App\Role::all();
    }

    public function activation(){
        return App\Activation::all();
    }

    public function relations(){
        #$rows = User::with('activation', 'roles')->get();
        $rows = User::join('activations', 'activations.id', '=', 'users.id')
                    ->join('role_user', 'role_user.user_id', '=', 'users.id')
                    ->join('roles', 'roles.id', '=', 'role_user.role_id')
                    ->orderBy('roles.id')
                    ->get();
        // get the role name.
        #return $rows->first()->name; // by just using the name field
        return $rows;
    }

    public function sortable(Request $request){
        $requested_to_sort_by = $request->get('sort_by');
        $sort_by = $request->has('sort_by') && array_key_exists($requested_to_sort_by, $this->sortable_fields) ? $this->sortable_fields[$requested_to_sort_by] : 'users.id';
        $rows = User::join('activations', 'activations.id', '=', 'users.id')
                    ->join('role_user', 'role_user.user_id', '=', 'users.id')
                    ->join('roles', 'roles.id', '=', 'role_user.role_id')
                    ->orderBy($sort_by)
                    ->get();
        return $rows;
    }

    public function paginated(){
        #$rows = User::with('activation', 'roles')->get();
        $rows = User::join('activations', 'activations.id', '=', 'users.id')
                    ->join('role_user', 'role_user.user_id', '=', 'users.id')
                    ->join('roles', 'roles.id', '=', 'role_user.role_id')
                    ->orderBy('roles.id')
                    ->paginate(10);
        return $rows;
    }

    public function search(Request $request){
        $rows = User::searchUsername($request->get('username'))
                    ->searchEmail($request->get('email'))
                    ->searchFirstName($request->get('first_name'))
                    ->searchLastName($request->get('last_name'))
                    ->get();
        return $rows;
    }

    public function xml(){
        $xml_path = public_path('xml.xml');
        $xml = \File::get($xml_path);
        $parsed = \Parser::xml($xml);
        return $parsed;
    }
}
